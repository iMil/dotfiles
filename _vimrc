set nocompatible " be iMproved, required for Vundle
filetype off " required
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'
"Plugin 'JamshedVesuna/vim-markdown-preview'
Plugin 'iamcco/markdown-preview.vim'
Plugin 'fatih/vim-go'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'
Plugin 'severin-lemaignan/vim-minimap'
Plugin 'Raimondi/delimitMate'
Plugin 'tpope/vim-fugitive'
Plugin 'scrooloose/nerdtree'
"Plugin 'ervandew/supertab'
Plugin 'SirVer/ultisnips'
" Snippets are separated from the engine. Add this if you want them:
Plugin 'honza/vim-snippets'

Plugin 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plugin 'junegunn/fzf.vim'
Plugin 'echuraev/translate-shell.vim'
Plugin 'madox2/vim-ai'
Plugin 'jamessan/vim-gnupg'

" All of your Plugins must be added before the following line
call vundle#end()
filetype plugin indent on
filetype plugin on

" end of Vundle configuration

set ts=8
set noai
set ruler
set tw=80
set formatoptions+=t
syntax on
set backspace=2
set background=dark
set t_Co=256
let g:molokai_original=1
colorscheme molokai
set number
set incsearch
set hlsearch
set guioptions-=m  "remove menu bar
set guioptions-=r  "remove scrollbar
set guioptions+=d  "remove scrollbar
set autochdir
set mouse-=a
if has('unnamedplus')
  set clipboard^=unnamed
  set clipboard^=unnamedplus
endif

let mapleader=","

au BufRead,BufNewFile pico.* setfiletype text
au BufRead,BufNewFile *.sls setfiletype yaml
au BufRead,BufNewFile *.md,*.markdown setlocal filetype=markdown
au BufNewFile,BufRead pf.conf   setf pf
au BufNewFile,BufRead pf.conf.* setf pf

au FileType c colorscheme molokai
au FileType go set tabstop=4 shiftwidth=4
au FileType python
    \ set tabstop=4 softtabstop=4 shiftwidth=4 textwidth=79 expandtab autoindent fileformat=unix
au FileType markdown set tw=0 spell
au FileType dockerfile set ts=4 expandtab
au FileType yaml set ts=2 expandtab
au FileType html set ts=2 expandtab nowrap
au FileType rust set ts=4 expandtab nosi
" rust customizations
autocmd BufNewFile,BufRead *.rs set formatprg=rustfmt

if !has('nvim')
	set list listchars=tab:�\ 
endif

let local_vimrc="/home/imil/.vim/vimrc"

if filereadable(local_vimrc)
	execute 'source '.local_vimrc
endif

if has("gui_running")
	set guifont=Hack\ 9
	set guioptions-=T
	set go-=L
	set colorcolumn=80
	hi ColorColumn guibg=#404040
	set linespace=2
	set lines=60 columns=120
endif

map <Esc>[1;5D <C-Left>
map <Esc>[1;5C <C-Right>
map! <Esc>[1;5D <C-Left>
map! <Esc>[1;5C <C-Right>

noremap <C-t> :tabnew<CR>
noremap <C-right> :tabnext<CR>
noremap <C-left> :tabprevious<CR>
noremap <C-l> :tabnext<CR>
noremap <C-k> :tabprevious<CR>

nmap <C-s> :set spell spelllang=
imap <C-s> [//]: # ( vim: set spell spelllang= : )

nmap <C-f> :Files<CR>
nmap <C-p> :History<CR>

"nmap <silent> <C-P> <Plug>MarkdownPreview

nmap <C-n> :NERDTreeToggle<CR>
let g:NERDTreeWinPos = "right"
" autostart NERDTree (annoying)
"autocmd VimEnter * NERDTree

nnoremap <leader>a :AI<CR>
xnoremap <leader>a :AI<CR>

if !exists(":Vterm")
	command Vterm vert botright term
endif

set guicursor=a:blinkon0
"let g:pep8_map='<C-P>'

set laststatus=2
let g:airline_theme='serene'
let g:airline_powerline_fonts=1
set encoding=utf-8

let $PATH.=':'.$HOME.'/.local/bin:'.$HOME.'/pkg/go/bin:'.$HOME.'/bin:'.$HOME.'.cargo/bin'
let $GOPATH=$HOME.'/pkg/gopath'
let $GOBIN=expand("$GOPATH/bin")

let g:go_def_mapping_enabled=0
let g:go_fmt_command = "goimports"
let g:go_list_type = "quickfix"

let g:go_highlight_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_function_calls = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_generate_tags = 1

let g:vim_markdown_folding_disabled = 1

let g:SuperTabDefaultCompletionType = '<C-n>'
